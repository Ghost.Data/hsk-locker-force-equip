﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Verse;
using static hsk_locker_force_equip.Patches;

namespace hsk_locker_force_equip {
    public class ForceEquipLocker : Mod {
        public ForceEquipLocker(ModContentPack content) : base(content) {
            Harmony harmony = new Harmony("hsk-forceequip");
            harmony.Patch(AccessTools.Method(AccessTools.TypeByName("SK.ClothLocker"), "ClothChange"), null, new HarmonyMethod(AccessTools.Method(typeof(ClothLocker_ClothChange_Patch), "Postfix")), null);

        }
    }
}
